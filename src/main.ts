import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';
// import { bootstrap }    from '@angular/platform-browser-dynamic';
// import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { HttpModule, RequestOptions } from '@angular/http';
import { AppComponent } from './app/app.component';
import * as jQuery from 'jquery';
// import { AppModule } from './app.module';
// import '@./styles.css'
platformBrowserDynamic().bootstrapModule(AppModule);
platformBrowserDynamic().bootstrapModule(HttpModule);
// bootstrap(AppComponent, [HttpModule]);

if (environment.production) {
  enableProdMode();
}

platformBrowserDynamic().bootstrapModule(AppModule)
  .catch(err => console.log(err));
