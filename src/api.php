<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

// $conn = new mysqli("localhost", "root", "root", "Library");

// $result = $conn->query("SELECT id, name, author, editor, resume, publication_date, genre FROM Books");

// $outp = "";
// while($rs = $result->fetch_array(MYSQLI_ASSOC)) {
//     if ($outp != "") {$outp .= ",";}
//     $outp .= '{"id":"'  . $rs["id"] . '",';
//     $outp .= '"name":"'   . $rs["name"]        . '",';
//     $outp .= '"author":"'. $rs["author"]     . '",';
//     $outp .= '"editor":"'. $rs["editor"]     . '",';
//     $outp .= '"resume":"'. $rs["resume"]     . '",';
//     $outp .= '"publication_date":"'. $rs["publication_date"]     . '",';
//     $outp .= '"genre":"'. $rs["genre"]     . '"}';
// }
// $outp ='{"records":['.$outp.']}';
// $conn->close();

// echo($outp);


// $user = 'root';
// $password = 'root';

// $db = new PDO('mysql:host=localhost;dbname=Library', $user, $password);
// // $outp = "";
// foreach ($db->query('SELECT * FROM Books') as $row) 
// {
//   $book->id = $row["id"];
//   $book->name  = $row["name"];
//   $book->author = $row["author"];
//   $book->editor = $row["editor"];
//   $book->publication_date = $row["publication_date"];
//     $json = json_encode($book);
//     echo $json;
//     $dec = json_decode($json);
//     // echo $dec;
// }

 	require_once("Rest.inc.php");
	
	class API extends REST {
	
		public $data = "";
		
		const DB_SERVER = "127.0.0.1";
		const DB_USER = "root";
		const DB_PASSWORD = "root";
		const DB = "Library";
		private $db = NULL;
		private $mysqli = NULL;
		public function __construct(){
			parent::__construct();				// Init parent contructor
			$this->dbConnect();					// Initiate Database connection
		}
		
		/*
		 *  Connect to Database
		*/
		private function dbConnect(){
			$this->mysqli = new mysqli(self::DB_SERVER, self::DB_USER, self::DB_PASSWORD, self::DB);
		}
		
		/*
		 * Dynmically call the method based on the query string
		 */
		public function processApi(){
			$func = strtolower(trim(str_replace("/","",$_REQUEST['x'])));
			if((int)method_exists($this,$func) > 0)
				$this->$func();
			else
				$this->response('',404); // If the method not exist with in this class "Page not found".
		}
				
		// private function login(){
		// 	if($this->get_request_method() != "POST"){
		// 		$this->response('',406);
		// 	}
		// 	$email = $this->_request['email'];		
		// 	$password = $this->_request['pwd'];
		// 	if(!empty($email) and !empty($password)){
		// 		if(filter_var($email, FILTER_VALIDATE_EMAIL)){
		// 			$query="SELECT uid, name, email FROM users WHERE email = '$email' AND password = '".md5($password)."' LIMIT 1";
		// 			$r = $this->mysqli->query($query) or die($this->mysqli->error.__LINE__);
		// 			if($r->num_rows > 0) {
		// 				$result = $r->fetch_assoc();	
		// 				// If success everythig is good send header as "OK" and user details
		// 				$this->response($this->json($result), 200);
		// 			}
		// 			$this->response('', 204);	// If no records "No Content" status
		// 		}
		// 	}
			
		// 	$error = array('status' => "Failed", "msg" => "Invalid Email address or Password");
		// 	$this->response($this->json($error), 400);
		// }
		
		private function books(){	
			if($this->get_request_method() != "GET"){
				$this->response('',406);
			}
			$query="SELECT distinct id, name, author, editor, resume, publication_date, genre FROM Books ORDER BY id DESC";
			$r = $this->mysqli->query($query) or die($this->mysqli->error.__LINE__);
			if($r->num_rows > 0){
				$result = array();
				while($row = $r->fetch_assoc()){
					$result[] = $row;
				}
				$this->response($this->json($result), 200); // send user details
			}
			$this->response('',204);	// If no records "No Content" status
		}
		// private function customer(){	
		// 	if($this->get_request_method() != "GET"){
		// 		$this->response('',406);
		// 	}
		// 	$id = (int)$this->_request['id'];
		// 	if($id > 0){	
		// 		$query="SELECT distinct c.customerNumber, c.customerName, c.email, c.address, c.city, c.state, c.postalCode, c.country FROM angularcode_customers c where c.customerNumber=$id";
		// 		$r = $this->mysqli->query($query) or die($this->mysqli->error.__LINE__);
		// 		if($r->num_rows > 0) {
		// 			$result = $r->fetch_assoc();	
		// 			$this->response($this->json($result), 200); // send user details
		// 		}
		// 	}
		// 	$this->response('',204);	// If no records "No Content" status
		// }
		
		private function insertBook(){
			if($this->get_request_method() != "POST"){
				$this->response('',406);
			}
			$book = json_decode(file_get_contents("php://input"),true);
			$column_names = array('name', 'author', 'editor', 'resume', 'publication_date', 'genre');
			$keys = array_keys($book);
			$columns = '';
			$values = '';
			foreach($column_names as $desired_key){ // Check the customer received. If blank insert blank into the array.
			   if(!in_array($desired_key, $keys)) {
			   		$$desired_key = '';
				}else{
					$$desired_key = $customer[$desired_key];
				}
				$columns = $columns.$desired_key.',';
				$values = $values."'".$$desired_key."',";
			}
			$query = "INSERT INTO Books(".trim($columns,',').") VALUES(".trim($values,',').")";
			if(!empty($book)){
				$r = $this->mysqli->query($query) or die($this->mysqli->error.__LINE__);
				$success = array('status' => "Success", "msg" => "New book saved successfully.", "data" => $book);
				$this->response($this->json($success),200);
			}else
				$this->response('',204);	//"No Content" status
    }
    
		private function updateBook(){
			if($this->get_request_method() != "PUT"){
				$this->response('',406);
			}
			$book = json_decode(file_get_contents("php://input"),true);
			$id = (int)$book['id'];
			$column_names = array('name', 'author', 'editor', 'resume', 'publication_date', 'genre');
			$keys = array_keys($book['id']);
			$columns = '';
			$values = '';
			foreach($column_names as $desired_key){ // Check the customer received. If key does not exist, insert blank into the array.
			   if(!in_array($desired_key, $keys)) {
			   		$$desired_key = '';
				}else{
					$$desired_key = $book['id'][$desired_key];
				}
				$columns = $columns.$desired_key."='".$$desired_key."',";
			}
			$query = "UPDATE Books SET ".trim($columns,',')." WHERE id=$id";
			if(!empty($customer)){
				$r = $this->mysqli->query($query) or die($this->mysqli->error.__LINE__);
				$success = array('status' => "Success", "msg" => "Books no.".$id." Updated Successfully.", "data" => $book);
				$this->response($this->json($success),200);
			}else
				$this->response('',204);	// "No Content" status
		}
		
    private function deleteBook(){
			if($this->get_request_method() != "DELETE"){
				$this->response('',406);
			}
			$id = (int)$this->_request['id'];
			if($id > 0){				
				$query="DELETE FROM Books WHERE id = $id";
				$r = $this->mysqli->query($query) or die($this->mysqli->error.__LINE__);
				$success = array('status' => "Success", "msg" => "Successfully deleted this book.");
				$this->response($this->json($success),200);
			}else
				$this->response('',204);	// If no records "No Content" status
		}
		
		/*
		 *	Encode array into JSON
		*/
		private function json($data){
			if(is_array($data)){
				return json_encode($data);
			}
		}
	}
	
	// Initiiate Library
	
	$api = new API;
	$api->processApi();


	books();
?>


