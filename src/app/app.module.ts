import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { BookListComponentComponent } from './book-list-component/book-list-component.component';
import { BookDetailComponentComponent } from './book-detail-component/book-detail-component.component';

@NgModule({
  declarations: [
    AppComponent,
    // BookListComponentComponent,
    // BookDetailComponentComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

// const appRoutes: Routes = [
//   { path: 'list', component: BookListComponentComponent },
//   { path: 'book/:id',      component: BookDetailComponentComponent },
  // {
  //   path: 'heroes',
  //   component: HeroListComponent,
  //   data: { title: 'Heroes List' }
  // },
  // { path: '',
  //   redirectTo: '/heroes',
  //   pathMatch: 'full'
  // },
  // { path: '**', component: PageNotFoundComponent }
// ];

// @NgModule({
//   declarations: [
//     AppComponent,
    // BookListComponentComponent,
    // BookDetailComponentComponent
  // ],
  // imports: [
    // RouterModule.forRoot(
    //   appRoutes,
    //   { enableTracing: true } // <-- debugging purposes only
    // ),

//      BrowserModule,



//   ],
//   providers: [],
//   bootstrap: [AppComponent]

// })
// export class AppModule { }